#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include "blockwrites.h"

int
append_write(int fd, char *data, unsigned long bytecount,
		char blkbuf[BLKBUF_SIZE], unsigned long *current_blkbuf_len)
{
	unsigned long bytes_remain;
	unsigned long blocks_count;
	size_t n;
	if (bytecount < *current_blkbuf_len) {
		memcpy(blkbuf + BLKBUF_SIZE - *current_blkbuf_len,
				data, bytecount);
		*current_blkbuf_len -= bytecount;
		return 1;
	}
	memcpy(blkbuf + BLKBUF_SIZE - *current_blkbuf_len,
			data, *current_blkbuf_len);
	bytecount -= *current_blkbuf_len;
	data += *current_blkbuf_len;
	blocks_count = bytecount / BYTES_IN_BLOCK;
	bytes_remain = bytecount % BYTES_IN_BLOCK;
	if (blocks_count) {
		ssize_t n;
		struct iovec io_vec[2];
		io_vec[0].iov_base = blkbuf;
		io_vec[0].iov_len = BLKBUF_SIZE;
		io_vec[1].iov_base = data;
		io_vec[1].iov_len = blocks_count * BYTES_IN_BLOCK;
		if ((n = writev(fd, io_vec, 2)) == -1L) {
#ifdef _DEBUG
			fprintf(stderr, "\n[debug] sys vector write failure: fd = %i, n = %li\n", fd, n);
#endif
			return 0;
		}
		memcpy(blkbuf, data + blocks_count * BYTES_IN_BLOCK,
				bytes_remain);
		*current_blkbuf_len = BLKBUF_SIZE - bytes_remain;
		return 1;
	}
	n = write(fd, blkbuf, BLKBUF_SIZE);
	if (n != BLKBUF_SIZE) {
#ifdef _DEBUG
		fprintf(stderr, "\n[debug] sys write failure: fd = %i, n = %li\n", fd, n);
#endif
		return 0;
	}
	memcpy(blkbuf, data, bytes_remain);
	*current_blkbuf_len = BLKBUF_SIZE - bytes_remain;
	return 1;
}
