#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <curl/curl.h>

#include "blockwrites.h"

struct input_data {
	const char *filename;
	int outfd;
	int response_code;
	size_t downloaded_size;
	size_t total_bytes;
	char wrblock[BLKBUF_SIZE];
	unsigned long cursz;
};

struct easy_options {
	char errbuf[CURL_ERROR_SIZE];
	const char *url;
	size_t (*write_fn) (char *, size_t, size_t, void *);
	size_t (*header_fn) (char *, size_t, size_t, void*);
	void *callback_param;
	long resume_offset;
};

enum option_result {
	OPTION_SET_OK,
	OPTION_ERR_ERRBUF,
	OPTION_ERR_URL,
	OPTION_ERR_WRITEFN,
	OPTION_ERR_HEADERFN,
	OPTION_ERR_CBPARAM,
	OPTION_ERR_RESUME
};

static size_t
write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	struct input_data *input = (struct input_data *)userdata;
	int b_ok;
	assert(size == 1);
	b_ok = append_write(input->outfd, ptr, nmemb, input->wrblock,
			&input->cursz);
	if (!b_ok) {
		perror("write failure");
		return 0UL;
	}
	input->downloaded_size += nmemb;
	if (input->total_bytes >= 100UL) {
		unsigned long percent = input->downloaded_size /
			(input->total_bytes / 100UL);
		if (percent > 100)
			percent = 100;
		printf("\rdl %lu of %lu bytes [%lu %%]", input->downloaded_size,
			input->total_bytes, percent);
	} else {
		printf("\r%lu bytes downloaded", input->downloaded_size);
	}
	fflush(stdout);
	return nmemb;
}

static size_t
header_callback(char *buffer, size_t size, size_t nitems, void *userdata)
{
	struct input_data *input = (struct input_data *)userdata;
	size_t counter;
	const char header_name[] = "content-length: ";
	const char location_header[] = "location: ";
	const char response_line[] = "HTTP/";
	const size_t hlen = sizeof(header_name) - 1;
	char *p;

#ifdef _DEBUG
	assert(size == 1);
#else
	(void)size;
#endif
	if (nitems > sizeof(response_line) - 1 && memcmp(buffer,
				response_line, sizeof(response_line) - 1) == 0)
		{
			char *spc2;
			char *spc = memchr(buffer, ' ', nitems);
			if (spc &&
			(spc2 = memchr(spc + 1, ' ', nitems - ((spc + 1)
					- buffer)))) {
				*spc2 = '\0';
				input->response_code = atoi(spc + 1);
#ifdef _DEBUG
				printf("\nresponse code: %i\n",
						input->response_code);
#endif
			}
		}
#ifdef _DEBUG
	printf("header_callback(): processing \"%s\"\n", buffer);
#endif
	if (strcmp("\r\n", buffer))
	for (counter = 0, p = buffer; *p != ':' && counter < nitems;
			++ p, ++ counter)
		*p = tolower(*p);
	if (input->response_code >= 300 && input->response_code < 400) {
		if (nitems > sizeof(location_header) - 1 &&
				memcmp(buffer, location_header,
					sizeof(location_header) - 1) == 0) {
			char *rn = memchr(buffer, '\r', nitems);
			if (rn) {
				*rn = '\0';
				fprintf(stderr,
					"warning: redirection to \"%s\"\n",
					buffer + sizeof(location_header) - 1);
				input->outfd = STDERR_FILENO;
				return nitems;
			}
		}
	}
	if (nitems > hlen && memcmp(buffer, header_name, hlen) == 0) {
		char *rn = strchr(buffer, '\r');
		if (rn) {
			int x;
			*rn = '\0';
			x = atoi(buffer + hlen);
			if (x >= 0)
				input->total_bytes = (unsigned long)x;
#ifdef _DEBUG
			printf("[log] int_total_bytes => %i, input->total_bytes => %lu, "
				"content_length_ptr + size: %s\n",
			x, input->total_bytes, buffer + hlen - 1);
#endif
		}
	}
	if ((nitems == 2 || nitems == 3) && memcmp(buffer, "\r\n", 2) == 0) {
		if (input->response_code >= 200 && input->response_code < 300) {
			if ((input->outfd = open(input->filename,
				input->response_code != 206 ? (O_CREAT | O_WRONLY) : (O_WRONLY | O_APPEND),
						0644)) == -1) {
				fprintf(stderr, "can't open file %s: %s\n",
						input->filename,
						strerror(errno));
				return 0UL;
			}
		} else {
			fprintf(stderr, "resp code %i\n",
					input->response_code);
			input->outfd = STDERR_FILENO;
		}
	}
	return nitems;
}

static enum option_result
set_curl_options(CURL *easy, const struct easy_options *opts, CURLcode *code)
{
	if ((*code = curl_easy_setopt(easy, CURLOPT_ERRORBUFFER, opts->errbuf))
			!= CURLE_OK) return OPTION_ERR_ERRBUF;
	if ((*code = curl_easy_setopt(easy, CURLOPT_URL, opts->url)) !=
			CURLE_OK) return OPTION_ERR_URL;
	if ((*code = curl_easy_setopt(easy, CURLOPT_WRITEFUNCTION,
					opts->write_fn)) != CURLE_OK)
		return OPTION_ERR_WRITEFN;
	if ((*code = curl_easy_setopt(easy, CURLOPT_HEADERFUNCTION,
					opts->header_fn)) != CURLE_OK)
		return OPTION_ERR_HEADERFN;
	if ((*code = curl_easy_setopt(easy, CURLOPT_WRITEDATA,
					opts->callback_param)) != CURLE_OK ||
	(*code = curl_easy_setopt(easy, CURLOPT_HEADERDATA,
				  opts->callback_param)) != CURLE_OK)
		return OPTION_ERR_CBPARAM;
	if (opts->resume_offset > 0)
		if ((*code = curl_easy_setopt(easy, CURLOPT_RESUME_FROM,
				opts->resume_offset) != CURLE_OK))
			return OPTION_ERR_RESUME;
	return OPTION_SET_OK;
}

static int
copy_string(char *dest, const char *source, unsigned long destlen)
{
	size_t srclen = strnlen(source, destlen) + 1;
	if (srclen > destlen)
		return 0;
	memcpy(dest, source, srclen);
	return 1;
}

static int
flush_and_close(struct input_data *input)
{
	const size_t remain = BLKBUF_SIZE - input->cursz;
	int res = 1;
	size_t n = remain ? write(input->outfd, input->wrblock, remain) : 0;
	if (n != remain) {
		res = 0;
		perror("warning: error flushing buffers (write)");
	}
	if (close(input->outfd) == -1) {
		res = 0;
		perror("warning: error closing output file");
	}
	return res;
}

int
main(int argc, char **argv)
{
	char outfn[512];
	CURL *easy;
	CURLcode curl_code;
	struct input_data info;
	int exit_code = 1;
	const char *url_file;
	struct easy_options liboptions;
	enum option_result optres;
	struct stat stfile;
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <url> [<out filename>]\n", argv[0]);
		return 1;
	}
	liboptions.url = argv[1];
	liboptions.write_fn = write_callback;
	liboptions.header_fn = header_callback;
	liboptions.callback_param = &info;
	liboptions.resume_offset = 0;
	curl_code = curl_global_init(CURL_GLOBAL_ALL);
	if (curl_code != CURLE_OK) {
		fprintf(stderr, "curl_global_init failed: %i\n", curl_code);
		return 1;
	}
	if (argc == 3) {
		if (!copy_string(outfn, argv[2], sizeof(outfn))) {
			fprintf(stderr, "bad file name: %s\n", argv[2]);
			return 1;
		}
	} else {
		if ((url_file = strrchr(argv[1], '/')) == NULL
			|| url_file[1] == '\0'
			|| !copy_string(outfn, url_file + 1, sizeof(outfn))) {
				fprintf(stderr, "saving output of %s to %s\n",
					argv[1], "data.out");
				strcpy(outfn, "data.out");
		}
	}
	easy = curl_easy_init();
	if (easy == NULL) {
		fputs("curl_easy_init failed\n", stderr);
		goto _glbcln;
	}
	if (stat(outfn, &stfile) != -1 && S_ISREG(stfile.st_mode)) {
		liboptions.resume_offset = stfile.st_size;
		fprintf(stderr,
			"warning: resuming dl from offset %li\n",
			liboptions.resume_offset);
	}
	optres = set_curl_options(easy, &liboptions, &curl_code);
	if (optres != OPTION_SET_OK) {
		switch (optres) {
		case OPTION_ERR_ERRBUF:
			fputs("failed to set error buffer: ", stderr);
			break;
		case OPTION_ERR_URL:
			fputs("failed to set url: ", stderr);
			break;
		case OPTION_ERR_WRITEFN:
			fputs("failed to set write callback: ", stderr);
			break;
		case OPTION_ERR_HEADERFN:
			fputs("failed to set header callback: ", stderr);
			break;
		case OPTION_ERR_CBPARAM:
			fputs("failed to set callback parameter: ", stderr);
			break;
		default:
			fputs("unknown error: ", stderr);
		}
		fprintf(stderr, "%s\n", curl_easy_strerror(curl_code));
		goto _esycln;
	}
	if (curl_code != CURLE_OK) {
		fprintf(stderr, "set option failure: %s\n",
				curl_easy_strerror(curl_code));
		goto _esycln;
	}
	info.outfd = -1;
	info.filename = outfn;
	info.downloaded_size = 0UL;
	info.total_bytes = 0UL;
	info.cursz = BLKBUF_SIZE;
	curl_code = curl_easy_perform(easy);
	putchar('\n');
	if (curl_code != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform failure: %s\n",
				curl_easy_strerror(curl_code));
		goto _esycln;
	}
	flush_and_close(&info);
	exit_code = 0;
_esycln:
	curl_easy_cleanup(easy);
_glbcln:
	curl_global_cleanup();
	return exit_code;
}
