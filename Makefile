#CFLAGS=-D_DEBUG -D_GNU_SOURCE -g -O0 -pedantic -std=c89 -Wall -Werror -Wextra
CFLAGS=-D_GNU_SOURCE -Os -pedantic -std=c99 -Wall -Werror -Wextra
LIBS=-lcurl
OUTPUT=simple-dl


$(OUTPUT): dl.c blockwrites.c
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)


clean:
	rm -f $(OUTPUT)
