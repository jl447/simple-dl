#define BLKBUF_SIZE 4096UL
#define BYTES_IN_BLOCK 512UL

int
append_write(int fd, char *data, unsigned long bytecount,
		char blkbuf[BLKBUF_SIZE], unsigned long *current_blkbuf_len);
